#!/usr/bin/env ruby
# frozen-string-literal: true

require 'bundler/setup'
require 'prometheus/client'
require 'prometheus/client/helper/plain_file'
require 'fast_mmaped_file'

file = ::Prometheus::Client::Helper::PlainFile.new(ARGV[0])

if ARGV[1] == '--stats'
  puts "len: #{file.used}"
  puts "typ: #{file.type}"
  puts "agg: #{file.multiprocess_mode}"
  puts "pid: #{file.pid}"
else
  puts ::FastMmapedFile.to_metrics([[file.filepath, file.multiprocess_mode.to_sym, file.type.to_sym, file.pid]])
end
