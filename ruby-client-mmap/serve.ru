#!/usr/bin/env ruby
# frozen-string-literal: true
#
# Run:
# $ EXPORTER_METRICS_DIR=... bundle exec rackup serve.ru

require 'rack'
require 'prometheus/client/rack/collector'
require 'prometheus/client/rack/exporter'

unless ENV['EXPORTER_METRICS_DIR']
  puts "Do: export EXPORTER_METRICS_DIR=<path>"
  exit 1
end

Prometheus::Client.configure do |config|
  config.multiprocess_files_dir = ENV['EXPORTER_METRICS_DIR']
end

use Rack::Deflater, if: ->(env, status, headers, body) { body.any? && body[0].length > 512 }
use Prometheus::Client::Rack::Exporter

run ->(env) { [200, {'Content-Type' => 'text/html'}, ['OK']] }
