#!/usr/bin/env ruby
# frozen-string-literal: true

require 'bundler/setup'
require 'prometheus/client'

script_dir = Pathname.new(__FILE__).dirname
out_dir = File.realpath(File.join(script_dir, "..", "test", "data", "mmap", "generated"))

FileUtils.rm_f(Dir.glob("#{out_dir}/*.db"))

def usage
  puts "Usage:\n\t#{$PROGRAM_NAME} [counter|gauge|histogram]"
end

start_pid = 0
client = ::Prometheus::Client
client.configure do |config|
  config.multiprocess_files_dir = out_dir
  config.pid_provider = proc { "proc_#{start_pid}".tap { start_pid += 1 } }
end

labelset_1 = { label1: "str", label2: 10 }
labelset_2 = { label1: "str", label2: 20 }

registry = client.registry

case ARGV[0]
when "counter"
  registry.counter(:test_counter1, "Test counter 1").tap do |counter|
    counter.increment(labelset_1)
    counter.increment(labelset_2)
    counter.increment(labelset_2)
  end
  registry.counter(:test_counter2, "Test counter 2").tap do |counter|
    counter.increment(labelset_1.merge(label3: 0.0))
    counter.increment(labelset_2.merge(label3: 1.01212121212121212121212212))
  end
  fork do
    registry.get(:test_counter1).tap do |counter|
      counter.increment(labelset_1)
    end
    registry.counter(:test_counter3, "Test counter 3").tap do |counter|
      counter.increment(labelset_1)
    end
  end
when "gauge"
  [:all, :min, :max, :livesum].each do |agg|
    gauge = registry.gauge("test_gauge_#{agg}".to_sym, "Test gauge", {}, agg)
    gauge.set(labelset_1, 1)
    gauge.set(labelset_2, 2)
    fork do
      gauge.set(labelset_1, 5)
      gauge.set(labelset_2, 10)
    end
  end
when "histogram"
  hist = registry.histogram(:test_histogram, "Test histogram", {}, [1.0, 2.0, 3.0])
  hist.observe(labelset_1, 1.0)
  hist.observe(labelset_1, 1.99)
  hist.observe(labelset_2, 2.55)
  hist.observe(labelset_2, 4.0)
else
  usage
end
