package mmap

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/testhelpers"
)

func Test_ParseCounterFile(t *testing.T) {
	f, err := ParseFile(testhelpers.FixtureFilePath("mmap/counter_proc_0-0.db"))
	require.NoError(t, err)

	require.Equal(t, "counter", f.MetricType)
	require.Equal(t, "livesum", f.Aggregation)
	require.Equal(t, "proc_0", f.Pid)
	require.Equal(t, 376, int(f.ContentLen))

	require.Equal(t, []encodedSample{
		{json: `["test_counter1","test_counter1",["label1","label2"],["str",10]]`,
			value: 1.0},
		{json: `["test_counter1","test_counter1",["label1","label2"],["str",20]]`,
			value: 2.0},
		{json: `["test_counter2","test_counter2",["label1","label2","label3"],["str",10,0.0]]`,
			value: 1.0},
		{json: `["test_counter2","test_counter2",["label1","label2","label3"],["str",20,1.0121212121212122]]`,
			value: 1.0},
	}, f.Samples)
}

func Test_ParseGaugeAllFile(t *testing.T) {
	f, err := ParseFile(testhelpers.FixtureFilePath("mmap/gauge_all_proc_0-0.db"))
	require.NoError(t, err)

	require.Equal(t, "gauge", f.MetricType)
	require.Equal(t, "all", f.Aggregation)
	require.Equal(t, "proc_0", f.Pid)
	require.Equal(t, 168, int(f.ContentLen))

	require.Equal(t, []encodedSample{
		{json: `["test_gauge_all","test_gauge_all",["label1","label2"],["str",10]]`,
			value: 1.0},
		{json: `["test_gauge_all","test_gauge_all",["label1","label2"],["str",20]]`,
			value: 2.0},
	}, f.Samples)
}

func Test_ParseGaugeSumFile(t *testing.T) {
	f, err := ParseFile(testhelpers.FixtureFilePath("mmap/gauge_livesum_proc_3-0.db"))
	require.NoError(t, err)

	require.Equal(t, "gauge", f.MetricType)
	require.Equal(t, "livesum", f.Aggregation)
	require.Equal(t, "proc_3", f.Pid)
	require.Equal(t, 184, int(f.ContentLen))

	require.Equal(t, []encodedSample{
		{json: `["test_gauge_livesum","test_gauge_livesum",["label1","label2"],["str",10]]`,
			value: 1.0},
		{json: `["test_gauge_livesum","test_gauge_livesum",["label1","label2"],["str",20]]`,
			value: 2.0},
	}, f.Samples)
}

func Test_ParseHistogram(t *testing.T) {
	f, err := ParseFile(testhelpers.FixtureFilePath("mmap/histogram_proc_0-0.db"))
	require.NoError(t, err)

	require.Equal(t, "histogram", f.MetricType)
	require.Equal(t, "livesum", f.Aggregation)
	require.Equal(t, "proc_0", f.Pid)
	require.Equal(t, 1192, int(f.ContentLen))

	require.Equal(t, []encodedSample{
		{json: `["test_histogram","test_histogram_sum",["label1","label2"],["str",10]]`,
			value: 2.9900000000000002},
		{json: `["test_histogram","test_histogram_count",["label1","label2"],["str",10]]`,
			value: 2.0},
		{json: `["test_histogram","test_histogram_bucket",["label1","label2","le"],["str",10,"+Inf"]]`,
			value: 2.0},
		{json: `["test_histogram","test_histogram_bucket",["label1","label2","le"],["str",10,"1.0"]]`,
			value: 1.0},
		{json: `["test_histogram","test_histogram_bucket",["label1","label2","le"],["str",10,"2.0"]]`,
			value: 2.0},
		{json: `["test_histogram","test_histogram_bucket",["label1","label2","le"],["str",10,"3.0"]]`,
			value: 2.0},
		{json: `["test_histogram","test_histogram_sum",["label1","label2"],["str",20]]`,
			value: 6.55},
		{json: `["test_histogram","test_histogram_count",["label1","label2"],["str",20]]`,
			value: 2.0},
		{json: `["test_histogram","test_histogram_bucket",["label1","label2","le"],["str",20,"+Inf"]]`,
			value: 2.0},
		{json: `["test_histogram","test_histogram_bucket",["label1","label2","le"],["str",20,"1.0"]]`,
			value: 0.0},
		{json: `["test_histogram","test_histogram_bucket",["label1","label2","le"],["str",20,"2.0"]]`,
			value: 0.0},
		{json: `["test_histogram","test_histogram_bucket",["label1","label2","le"],["str",20,"3.0"]]`,
			value: 1.0},
	}, f.Samples)
}

func Test_ReturnErrorWhenMetricTypeNotSupported(t *testing.T) {
	_, err := ParseFile(testhelpers.FixtureFilePath("mmap/invalid/does_not_match"))

	require.Error(t, err)
	require.Equal(t, "Unexpected file name: does_not_match", err.Error())
}
