package mmap

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_GroupFiles(t *testing.T) {
	var files []string

	counters := []string{
		"counter_proc_0-0.db",
		"counter_proc_1-0.db",
	}
	files = append(files, counters...)

	gaugeAll := []string{
		"gauge_all_proc_0-0.db",
		"gauge_all_proc_1-0.db",
	}
	files = append(files, gaugeAll...)

	gaugeLivesum := []string{
		"gauge_livesum_proc_0-0.db",
		"gauge_livesum_proc_1-0.db",
	}
	files = append(files, gaugeLivesum...)

	gaugeMax := []string{
		"gauge_max_proc_0-0.db",
		"gauge_max_proc_1-0.db",
	}
	files = append(files, gaugeMax...)

	gaugeMin := []string{
		"gauge_min_proc_0-0.db",
		"gauge_min_proc_1-0.db",
	}
	files = append(files, gaugeMin...)

	histogram := []string{
		"histogram_proc_0-0.db",
	}
	files = append(files, histogram...)

	res, err := GroupFiles(files)
	require.NoError(t, err)

	expectedGroups := map[FileGroup][]string{
		{
			Typ: "counter",
			Agg: "livesum",
		}: counters,
		{
			Typ: "gauge",
			Agg: "all",
		}: gaugeAll,
		{
			Typ: "gauge",
			Agg: "livesum",
		}: gaugeLivesum,
		{
			Typ: "gauge",
			Agg: "max",
		}: gaugeMax,
		{
			Typ: "gauge",
			Agg: "min",
		}: gaugeMin,
		{
			Typ: "histogram",
			Agg: "livesum",
		}: histogram,
	}
	require.True(t, reflect.DeepEqual(expectedGroups, res))
}
