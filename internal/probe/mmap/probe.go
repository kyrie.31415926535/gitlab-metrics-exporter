package mmap

import (
	"fmt"
	"io/fs"
	"os"
	"path"
	"sync"

	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
	"gitlab.com/gitlab-org/labkit/log"
)

type mmapdbProbe struct {
	path string
	glob string
}

type visitedEntriesKey struct {
	encodedMetric string
	pid           string
}

type visitedEntriesMap map[visitedEntriesKey]float64

func NewDefaultProbe(mmapDir string) probe.Probe {
	return &mmapdbProbe{mmapDir, "*.db"}
}

func NewProbe(mmapDir string, glob string) probe.Probe {
	return &mmapdbProbe{mmapDir, glob}
}

func (p *mmapdbProbe) String() string {
	return "mmap"
}

func (p *mmapdbProbe) Probe(ch chan<- probe.ProbeResult) error {
	fileSystem := os.DirFS(p.path)
	files, err := fs.Glob(fileSystem, p.glob)
	if err != nil {
		return err
	}

	fileGroups, err := GroupFiles(files)
	if err != nil {
		return err
	}

	// Map and parse each group of paths to a MetricGroup.
	wg := sync.WaitGroup{}
	for k := range fileGroups {
		key := k
		wg.Add(1)
		go func() {
			defer wg.Done()

			p.processFileGroup(ch, key, fileGroups[key])
		}()
	}

	wg.Wait()

	return nil
}

// Processes a group of files that have the same metric type and aggregation.
func (p *mmapdbProbe) processFileGroup(ch chan<- probe.ProbeResult, groupKey FileGroup, filenames []string) {
	fileChannel := make(chan *MetricFile, 1) // allow to buffer a few files

	go p.parseFileGroupAsync(filenames, fileChannel, ch)

	var visitedEntries visitedEntriesMap

	for metricFile := range fileChannel {
		var significantPid string
		retainAll := metricFile.Aggregation == AggregateAll && metricFile.MetricType == probe.MetricTypeGauge
		if retainAll {
			significantPid = metricFile.Pid
		}

		if visitedEntries == nil {
			// preallocate map as all those samples will be stored in the map
			// give it 50% more space
			visitedEntries = make(visitedEntriesMap, len(metricFile.Samples)*150/100)
		}

		if retainAll || len(visitedEntries) == 0 {
			setSamples(visitedEntries, metricFile.Samples, significantPid)
			continue
		}

		switch metricFile.Aggregation {
		case AggregateMin:
			mergeByMin(visitedEntries, metricFile.Samples)
		case AggregateMax:
			mergeByMax(visitedEntries, metricFile.Samples)
		case AggregateSum:
			mergeBySum(visitedEntries, metricFile.Samples)
		default:
			ch <- probe.ProbeResult{Error: fmt.Errorf("unexpected aggregation: %s", metricFile.Aggregation)}
			return
		}
	}

	emitProbeResults(ch, groupKey, visitedEntries)
}

func (p *mmapdbProbe) parseFileGroupAsync(filenames []string, fileChannel chan *MetricFile, ch chan<- probe.ProbeResult) {
	for _, f := range filenames {
		filePath := path.Join(p.path, f)

		metricFile, err := ParseFile(filePath)
		if err != nil {
			ch <- probe.ProbeResult{Error: fmt.Errorf("ParseFile %s: %w", filePath, err)}
			return
		}

		fileChannel <- metricFile
	}

	close(fileChannel)
}

func emitProbeResults(ch chan<- probe.ProbeResult, groupKey FileGroup, visitedEntries visitedEntriesMap) {
	if len(visitedEntries) == 0 {
		return
	}

	samples := make(probe.SampleSlice, len(visitedEntries))
	mgroup := probe.MetricGroup{
		Type:    groupKey.Typ,
		Help:    "Multiprocess metric",
		Samples: samples,
	}

	i := 0
	for key, value := range visitedEntries {
		sample := probe.Sample{Value: value}
		if err := decodeMetric(&sample, key.encodedMetric, key.pid); err != nil {
			log.WithField("aggregation", groupKey.Agg).
				WithField("type", groupKey.Typ).
				Warn("dropping corrupted sample: %w", err)
			continue
		}
		samples[i] = sample
		i++
	}

	ch <- probe.ProbeResult{MGroup: mgroup, Error: nil}
}

func setSamples(visitedSamples visitedEntriesMap, fileSamples []encodedSample, pid string) {
	for _, sample := range fileSamples {
		visitedSamples[visitedEntriesKey{sample.json, pid}] = sample.value
	}
}

func mergeBySum(visitedSamples visitedEntriesMap, fileSamples []encodedSample) {
	for _, sample := range fileSamples {
		visitedSamples[visitedEntriesKey{sample.json, ""}] += sample.value
	}
}

func mergeByMin(visitedSamples visitedEntriesMap, fileSamples []encodedSample) {
	for _, sample := range fileSamples {
		key := visitedEntriesKey{sample.json, ""}
		if visitedSample, ok := visitedSamples[key]; !ok || sample.value < visitedSample {
			visitedSamples[key] = sample.value
		}
	}
}

func mergeByMax(visitedSamples visitedEntriesMap, fileSamples []encodedSample) {
	for _, sample := range fileSamples {
		key := visitedEntriesKey{sample.json, ""}
		if visitedSample, ok := visitedSamples[key]; !ok || sample.value > visitedSample {
			visitedSamples[key] = sample.value
		}
	}
}
