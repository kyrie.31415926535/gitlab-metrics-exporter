package mmap

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/testhelpers"
)

func Test_MergeCounterBySum(t *testing.T) {
	p := NewProbe(testhelpers.FixtureFilePath("mmap"), "counter_*")

	mgroups, err := probe.Await(p)

	require.NoError(t, err)
	require.Len(t, mgroups, 1)
	require.Equal(t, probe.MetricTypeCounter, mgroups[0].Type)
	require.Equal(t, "Multiprocess metric", mgroups[0].Help)
	require.ElementsMatch(t, probe.SampleSlice{
		{
			FamilyName: "test_counter1",
			MetricName: "test_counter1",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "10",
				},
			},
			Value: 2,
		},
		{
			FamilyName: "test_counter1",
			MetricName: "test_counter1",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "20",
				},
			},
			Value: 2,
		},
		{
			FamilyName: "test_counter2",
			MetricName: "test_counter2",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "10",
				},
				{
					Name:  "label3",
					Value: "0.0",
				},
			},
			Value: 1,
		},
		{
			FamilyName: "test_counter2",
			MetricName: "test_counter2",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "20",
				},
				{
					Name:  "label3",
					Value: "1.0121212121212122",
				},
			},
			Value: 1,
		},
		{
			FamilyName: "test_counter3",
			MetricName: "test_counter3",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "10",
				},
			},
			Value: 1,
		},
	}, mgroups[0].Samples)
}

func Test_MergeGaugeByMin(t *testing.T) {
	p := NewProbe(testhelpers.FixtureFilePath("mmap"), "gauge_min_*")

	mgroups, err := probe.Await(p)
	require.NoError(t, err)
	require.Len(t, mgroups, 1)
	require.Equal(t, probe.MetricTypeGauge, mgroups[0].Type)
	require.Equal(t, "Multiprocess metric", mgroups[0].Help)
	require.ElementsMatch(t, probe.SampleSlice{
		{
			FamilyName: "test_gauge_min",
			MetricName: "test_gauge_min",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "10",
				},
			},
			Value: 1,
		},
		{
			FamilyName: "test_gauge_min",
			MetricName: "test_gauge_min",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "20",
				},
			},

			Value: 2,
		},
	}, mgroups[0].Samples)
}

func Test_MergeGaugeByMax(t *testing.T) {
	p := NewProbe(testhelpers.FixtureFilePath("mmap"), "gauge_max_*")

	mgroups, err := probe.Await(p)
	require.NoError(t, err)
	require.Len(t, mgroups, 1)
	require.Equal(t, probe.MetricTypeGauge, mgroups[0].Type)
	require.Equal(t, "Multiprocess metric", mgroups[0].Help)
	require.ElementsMatch(t, probe.SampleSlice{
		{
			FamilyName: "test_gauge_max",
			MetricName: "test_gauge_max",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "10",
				},
			},
			Value: 5,
		},
		{
			FamilyName: "test_gauge_max",
			MetricName: "test_gauge_max",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "20",
				},
			},
			Value: 10,
		},
	}, mgroups[0].Samples)
}

func Test_MergeGaugeBySum(t *testing.T) {
	p := NewProbe(testhelpers.FixtureFilePath("mmap"), "gauge_livesum_*")

	mgroups, err := probe.Await(p)
	require.NoError(t, err)
	require.Len(t, mgroups, 1)
	require.Equal(t, probe.MetricTypeGauge, mgroups[0].Type)
	require.Equal(t, "Multiprocess metric", mgroups[0].Help)
	require.ElementsMatch(t, probe.SampleSlice{
		{
			FamilyName: "test_gauge_livesum",
			MetricName: "test_gauge_livesum",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "10",
				},
			},
			Value: 6,
		},
		{
			FamilyName: "test_gauge_livesum",
			MetricName: "test_gauge_livesum",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "20",
				},
			},
			Value: 12,
		},
	}, mgroups[0].Samples)
}

func Test_DoNotMergeGaugeWithAllAggregation(t *testing.T) {
	p := NewProbe(testhelpers.FixtureFilePath("mmap"), "gauge_all_*")

	mgroups, err := probe.Await(p)
	require.NoError(t, err)
	require.Len(t, mgroups, 1)
	require.Equal(t, probe.MetricTypeGauge, mgroups[0].Type)
	require.Equal(t, "Multiprocess metric", mgroups[0].Help)
	require.ElementsMatch(t, probe.SampleSlice{
		{
			FamilyName: "test_gauge_all",
			MetricName: "test_gauge_all",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "10",
				},
				{
					Name:  "pid",
					Value: "proc_0",
				},
			},
			Value: 1,
		},
		{
			FamilyName: "test_gauge_all",
			MetricName: "test_gauge_all",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "20",
				},
				{
					Name:  "pid",
					Value: "proc_0",
				},
			},
			Value: 2,
		},
		{
			FamilyName: "test_gauge_all",
			MetricName: "test_gauge_all",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "10",
				},
				{
					Name:  "pid",
					Value: "proc_1",
				},
			},
			Value: 5,
		},
		{
			FamilyName: "test_gauge_all",
			MetricName: "test_gauge_all",
			Labels: probe.LabelSlice{
				{
					Name:  "label1",
					Value: "str",
				},
				{
					Name:  "label2",
					Value: "20",
				},
				{
					Name:  "pid",
					Value: "proc_1",
				},
			},
			Value: 10,
		},
	}, mgroups[0].Samples)
}
