package mmap

import (
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
)

// decodeMetric takes metric data encoded as a nested JSON array using the following format:
// [metric_name, child_metric_name, [label_a, label_b, ...], [label_a_value, label_b_value, ...]]
//
// The internal JSON decoder uses reflection, which makes it slow.
// We take advantage of knowing the precise structure of an encoded entry upfront
// and process the string manually instead. Plus, since label values will be
// rendered to strings again anyway, there is no need to first convert them
// to Golang data types.
func decodeMetric(sample *probe.Sample, encodedMetric string, significantPid string) error {
	var err error

	// remove leading '[' and trailing ']]'
	strippedMetric := encodedMetric[1 : len(encodedMetric)-2]

	// This should yield the 3 expected parts (so, two occurrences of `[`): names, labelNames, labelValues
	partsCount := strings.Count(strippedMetric, "[")
	if partsCount != 2 {
		return fmt.Errorf("unexpected format in sample '%s'", encodedMetric)
	}

	metricNames := nextToken(&strippedMetric, '[')

	name1 := nextToken(&metricNames, ',')
	name1 = name1[1 : len(name1)-1]
	sample.FamilyName = name1

	name2 := nextToken(&metricNames, ',')
	name2 = name2[1 : len(name2)-1]
	sample.MetricName = name2

	pidIndex := -1

	labelNameStr := nextToken(&strippedMetric, '[')

	sample.Labels = sample.Labels[:0]

	if labelNameStr == "]," {
		// empty label array i.e. '[]' token
	} else {
		// drop the trailing '],'
		labelNameStr = labelNameStr[:len(labelNameStr)-2]
		labelValueStr := strippedMetric
		pidIndex, err = decodeLabels(&sample.Labels, labelNameStr, labelValueStr)
		if err != nil {
			return fmt.Errorf("unexpected format in sample '%s': %w", encodedMetric, err)
		}
	}

	// Insert a `pid` label for gauges with aggregation ALL, unless one already exists.
	// This is because they will not be aggregated across files, so must be distinct
	// per process.
	if pidIndex < 0 && significantPid != "" {
		sample.Labels = append(sample.Labels, probe.Label{Name: "pid", Value: significantPid})
	}

	return nil
}

func nextToken(value *string, sep byte) string {
	idx := strings.IndexByte(*value, sep)

	if idx > 0 {
		result := (*value)[0:idx]
		*value = (*value)[idx+1:]
		return result
	}

	result := *value
	*value = ""
	return result
}

func decodeLabels(labels *probe.LabelSlice, labelNameStr string, labelValueStr string) (int, error) {
	labelNamesCount := strings.Count(labelNameStr, ",") + 1
	labelValueStrCount := strings.Count(labelValueStr, ",") + 1
	if labelNamesCount != labelValueStrCount {
		return -1, fmt.Errorf("label names and values cardinality mismatch")
	}

	pidIndex := -1
	for i := 0; i < labelNamesCount; i++ {
		ln := nextToken(&labelNameStr, ',')

		// Normalize label names.
		ln = ln[1 : len(ln)-1] // strip quotes
		if ln == "pid" {
			pidIndex = i
		}

		lv := nextToken(&labelValueStr, ',')
		if lv[0] == '"' {
			lv = lv[1 : len(lv)-1] // strip quotes
		} else if lv == "null" {
			lv = ""
		}

		*labels = append(*labels, probe.Label{Name: ln, Value: lv})
	}

	return pidIndex, nil
}
