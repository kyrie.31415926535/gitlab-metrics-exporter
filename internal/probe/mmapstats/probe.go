package mmapstats

import (
	"io/fs"
	"os"

	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe/mmap"
)

const (
	mmapFilesSizeBytesMetricName = probe.MetricsNamespace + "_mmap_files_size_bytes"
	mmapFilesSizeBytesMetricHelp = "Mmapdb files sizes grouped by (metric type, aggregation)"
	mmapFilesMetricName          = probe.MetricsNamespace + "_mmap_files"
	mmapFilesMetricHelp          = "Mmapdb files counts grouped by (metric type, aggregation)"
)

type mmapStatsProbe struct {
	path string
	glob string
}

func NewProbe(mmapDir string) probe.Probe {
	return &mmapStatsProbe{mmapDir, "*.db"}
}

func (p *mmapStatsProbe) String() string {
	return "mmap_stats"
}

func (p *mmapStatsProbe) Probe(ch chan<- probe.ProbeResult) error {
	fileSystem := os.DirFS(p.path)
	files, err := fs.Glob(fileSystem, p.glob)
	if err != nil {
		return err
	}

	fileGroups, err := mmap.GroupFiles(files)
	if err != nil {
		return err
	}

	probeFilesCounts(ch, fileGroups)
	probeFilesSizes(ch, fileGroups, fileSystem)

	return nil
}

func probeFilesCounts(ch chan<- probe.ProbeResult, fileGroups map[mmap.FileGroup][]string) {
	samples := make(probe.SampleSlice, 0, len(fileGroups))

	for fileGroup, files := range fileGroups {
		labels := probe.LabelSlice{
			probe.Label{Name: "metric_type", Value: fileGroup.Typ},
			probe.Label{Name: "aggregation", Value: fileGroup.Agg},
		}

		sample := probe.Sample{
			FamilyName: mmapFilesMetricName,
			MetricName: mmapFilesMetricName,
			Labels:     labels,
			Value:      float64(len(files))}

		samples = append(samples, sample)
	}

	mgroup := probe.MetricGroup{
		Type:    "gauge",
		Help:    mmapFilesMetricHelp,
		Samples: samples,
	}

	ch <- probe.ProbeResult{MGroup: mgroup, Error: nil}
}

func probeFilesSizes(ch chan<- probe.ProbeResult, fileGroups map[mmap.FileGroup][]string, fileSystem fs.FS) {
	samples := make(probe.SampleSlice, 0, len(fileGroups))

	for fileGroup, files := range fileGroups {
		var sum int64
		for _, file := range files {
			stat, err := fs.Stat(fileSystem, file)
			if err != nil {
				ch <- probe.ProbeResult{Error: err}
			}
			sum += stat.Size()
		}

		labels := probe.LabelSlice{
			probe.Label{Name: "metric_type", Value: fileGroup.Typ},
			probe.Label{Name: "aggregation", Value: fileGroup.Agg},
		}

		sample := probe.Sample{
			FamilyName: mmapFilesSizeBytesMetricName,
			MetricName: mmapFilesSizeBytesMetricName,
			Labels:     labels,
			Value:      float64(sum)}

		samples = append(samples, sample)
	}

	mgroup := probe.MetricGroup{
		Type:    "gauge",
		Help:    mmapFilesSizeBytesMetricHelp,
		Samples: samples,
	}

	ch <- probe.ProbeResult{MGroup: mgroup, Error: nil}
}
