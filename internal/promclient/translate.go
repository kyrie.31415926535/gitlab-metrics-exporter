package promclient

import (
	"fmt"
	"strings"

	prom "github.com/prometheus/client_model/go"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
)

func MetricFamiliesToMetricGroups(mfs []*prom.MetricFamily) ([]probe.MetricGroup, error) {
	mgroups := make([]probe.MetricGroup, len(mfs))
	for i, mf := range mfs {
		group, err := MetricFamilyToMetricGroup(mf)
		if err != nil {
			return nil, err
		}
		mgroups[i] = *group
	}
	return mgroups, nil
}

func MetricFamilyToMetricGroup(mf *prom.MetricFamily) (*probe.MetricGroup, error) {
	mgroup := probe.MetricGroup{}
	mgroup.Type = strings.ToLower(mf.GetType().String())
	mgroup.Help = mf.GetHelp()

	switch *mf.Type {
	case prom.MetricType_COUNTER:
		mgroup.Samples = simpleMetricSamples(mf, func(m *prom.Metric) float64 { return m.GetCounter().GetValue() })
	case prom.MetricType_GAUGE:
		mgroup.Samples = simpleMetricSamples(mf, func(m *prom.Metric) float64 { return m.GetGauge().GetValue() })
	case prom.MetricType_HISTOGRAM:
		mgroup.Samples = histogramSamples(mf)
	case prom.MetricType_SUMMARY:
		mgroup.Samples = summarySamples(mf)
	default:
		return nil, fmt.Errorf("unsupported metric type: %s", mf.Type.String())
	}

	return &mgroup, nil
}

func simpleMetricSamples(mf *prom.MetricFamily, valueFn func(m *prom.Metric) float64) probe.SampleSlice {
	samples := make(probe.SampleSlice, len(mf.GetMetric()))
	for i, m := range mf.GetMetric() {
		samples[i] = probe.Sample{
			FamilyName: mf.GetName(),
			MetricName: mf.GetName(),
			Labels:     transformLabels(m.GetLabel()),
			Value:      valueFn(m),
		}
	}
	return samples
}

func histogramSamples(mf *prom.MetricFamily) probe.SampleSlice {
	var samples probe.SampleSlice
	for _, m := range mf.GetMetric() {
		h := m.GetHistogram()

		// Map buckets to samples.
		for _, b := range h.GetBucket() {
			samples = append(samples, probe.Sample{
				FamilyName: mf.GetName(),
				MetricName: mf.GetName() + "_bucket",
				Labels: transformLabels(
					m.GetLabel(), &probe.Label{Name: "le", Value: internal.FormatCompactFloat(b.GetUpperBound())},
				),
				Value: float64(b.GetCumulativeCount()),
			})
		}

		// Prometheus client does not add an +Inf bucket automatically.
		// It must always equal the total sample count.
		samples = append(samples, probe.Sample{
			FamilyName: mf.GetName(),
			MetricName: mf.GetName() + "_bucket",
			Labels: transformLabels(
				m.GetLabel(), &probe.Label{Name: "le", Value: "+Inf"},
			),
			Value: float64(h.GetSampleCount()),
		})

		// Map sum and count metrics.
		samples = append(samples, probe.Sample{
			FamilyName: mf.GetName(),
			MetricName: mf.GetName() + "_sum",
			Labels:     transformLabels(m.GetLabel()),
			Value:      h.GetSampleSum(),
		})
		samples = append(samples, probe.Sample{
			FamilyName: mf.GetName(),
			MetricName: mf.GetName() + "_count",
			Labels:     transformLabels(m.GetLabel()),
			Value:      float64(h.GetSampleCount()),
		})
	}
	return samples
}

func summarySamples(mf *prom.MetricFamily) probe.SampleSlice {
	var samples probe.SampleSlice
	for _, m := range mf.GetMetric() {
		s := m.GetSummary()

		// Map buckets to samples.
		for _, q := range s.GetQuantile() {
			samples = append(samples, probe.Sample{
				FamilyName: mf.GetName(),
				MetricName: mf.GetName(),
				Labels: transformLabels(
					m.GetLabel(), &probe.Label{Name: "quantile", Value: internal.FormatCompactFloat(q.GetQuantile())},
				),
				Value: q.GetValue(),
			})
		}

		// Map sum and count metrics.
		samples = append(samples, probe.Sample{
			FamilyName: mf.GetName(),
			MetricName: mf.GetName() + "_sum",
			Labels:     transformLabels(m.GetLabel()),
			Value:      s.GetSampleSum(),
		})
		samples = append(samples, probe.Sample{
			FamilyName: mf.GetName(),
			MetricName: mf.GetName() + "_count",
			Labels:     transformLabels(m.GetLabel()),
			Value:      float64(s.GetSampleCount()),
		})
	}
	return samples
}

func transformLabels(inLabels []*prom.LabelPair, extraLabels ...*probe.Label) probe.LabelSlice {
	outLabels := make(probe.LabelSlice, len(inLabels)+len(extraLabels))
	for i, l := range inLabels {
		outLabels[i] = probe.Label{Name: l.GetName(), Value: l.GetValue()}
	}
	j := len(inLabels)
	for _, l := range extraLabels {
		outLabels[j] = *l
		j++
	}
	return outLabels
}
