package render

import (
	"io"

	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
)

type Renderer interface {
	TextAsync(ch <-chan probe.ProbeResult, w io.Writer) error
	Text(mgroups []probe.MetricGroup, w io.Writer) error
}

type renderer struct{}

func NewRenderer() Renderer {
	return &renderer{}
}
