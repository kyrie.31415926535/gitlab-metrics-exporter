package render

import (
	"bufio"
	"io"
	"sort"

	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
)

func (r *renderer) TextAsync(ch <-chan probe.ProbeResult, w io.Writer) error {
	bw := bufio.NewWriterSize(w, 32*1024)
	defer func() {
		_ = bw.Flush()
	}()

	observedMetrics := make(map[string]struct{}, 100)

	for res := range ch {
		if res.Error != nil {
			return res.Error
		}
		if err := r.appendMetricGroup(bw, &res.MGroup, observedMetrics); err != nil {
			return err
		}
	}

	return nil
}

// Text serializes a list of metric groups to the Prometheus Text Exposition Format.
//
//	See https://prometheus.io/docs/instrumenting/exposition_formats/#text-based-format
//
//		metric_name [
//		"{" label_name "=" `"` label_value `"` { "," label_name "=" `"` label_value `"` } [ "," ] "}"
//		] value [ timestamp ]
func (r *renderer) Text(mgroups []probe.MetricGroup, w io.Writer) error {
	ch := make(chan probe.ProbeResult)
	go func() {
		for _, mgroup := range mgroups {
			ch <- probe.ProbeResult{MGroup: mgroup}
		}
		close(ch)
	}()

	return r.TextAsync(ch, w)
}

func (r *renderer) appendMetricGroup(w io.Writer, mg *probe.MetricGroup, observedMetrics map[string]struct{}) error {
	var err error
	sb := make([]byte, 0, 4096)

	sort.Sort(mg.Samples)

	for _, sample := range mg.Samples {
		if _, ok := observedMetrics[sample.FamilyName]; !ok {
			sb = sb[:0]
			sb = append(sb, "# HELP "...)
			sb = append(sb, sample.FamilyName...)
			sb = append(sb, " "...)
			sb = append(sb, mg.Help...)
			sb = append(sb, "\n"...)

			sb = append(sb, "# TYPE "...)
			sb = append(sb, sample.FamilyName...)
			sb = append(sb, " "...)
			sb = append(sb, mg.Type...)
			sb = append(sb, "\n"...)
			if _, err = w.Write(sb); err != nil {
				return err
			}

			observedMetrics[sample.FamilyName] = struct{}{}
		}

		sb = sb[:0]
		sb = append(sb, sample.MetricName...)
		appendLabels(&sb, sample.Labels)
		sb = append(sb, " "...)
		sb = internal.AppendPreciseFloat(sb, sample.Value)
		sb = append(sb, "\n"...)
		if _, err = w.Write(sb); err != nil {
			return err
		}
	}

	return nil
}

// From golang: https://cs.opensource.google/go/go/+/refs/tags/go1.17.6:src/sort/sort.go;drc=refs%2Ftags%2Fgo1.17.6;l=34
// To not allocate memory
func insertionSort(data probe.LabelSlice, a, b int) {
	for i := a + 1; i < b; i++ {
		for j := i; j > a && data.Less(j, j-1); j-- {
			data.Swap(j, j-1)
		}
	}
}

func appendLabels(sb *[]byte, labels probe.LabelSlice) {
	if len(labels) == 0 {
		return
	}

	insertionSort(labels, 0, len(labels))

	*sb = append(*sb, "{"...)
	for i, lv := range labels {
		if i > 0 {
			*sb = append(*sb, "\","...)
		}
		*sb = append(*sb, lv.Name...)
		*sb = append(*sb, "=\""...)
		*sb = append(*sb, lv.Value...)
	}
	*sb = append(*sb, "\"}"...)
}
