package render

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/testhelpers"
)

func Test_RenderCounterText(t *testing.T) {
	expectedOutput :=
		`# HELP test_counter1 Multiprocess metric
# TYPE test_counter1 counter
test_counter1{label1="str",label2="10"} 2
test_counter1{label1="str",label2="20"} 2
# HELP test_counter2 Multiprocess metric
# TYPE test_counter2 counter
test_counter2{label1="str",label2="10",label3="0.0"} 1
test_counter2{label1="str",label2="20",label3="1.0121212121212122"} 1
# HELP test_counter3 Multiprocess metric
# TYPE test_counter3 counter
test_counter3{label1="str",label2="10"} 1
`

	output, err := MmapFilesText(testhelpers.FixtureFilePath("mmap"), "counter_*")
	require.NoError(t, err)
	require.Equal(t, expectedOutput, output)
}

func Test_RenderGaugeText(t *testing.T) {
	expectedOutput :=
		`# HELP test_gauge_all Multiprocess metric
# TYPE test_gauge_all gauge
test_gauge_all{label1="str",label2="10",pid="proc_0"} 1
test_gauge_all{label1="str",label2="10",pid="proc_1"} 5
test_gauge_all{label1="str",label2="20",pid="proc_0"} 2
test_gauge_all{label1="str",label2="20",pid="proc_1"} 10
`

	output, err := MmapFilesText(testhelpers.FixtureFilePath("mmap"), "gauge_all_*")
	require.NoError(t, err)
	require.Equal(t, expectedOutput, output)
}

func Test_RenderHistogramText(t *testing.T) {
	expectedOutput :=
		`# HELP test_histogram Multiprocess metric
# TYPE test_histogram histogram
test_histogram_bucket{label1="str",label2="10",le="+Inf"} 2
test_histogram_bucket{label1="str",label2="10",le="1.0"} 1
test_histogram_bucket{label1="str",label2="10",le="2.0"} 2
test_histogram_bucket{label1="str",label2="10",le="3.0"} 2
test_histogram_bucket{label1="str",label2="20",le="+Inf"} 2
test_histogram_bucket{label1="str",label2="20",le="1.0"} 0
test_histogram_bucket{label1="str",label2="20",le="2.0"} 0
test_histogram_bucket{label1="str",label2="20",le="3.0"} 1
test_histogram_count{label1="str",label2="10"} 2
test_histogram_count{label1="str",label2="20"} 2
test_histogram_sum{label1="str",label2="10"} 2.9900000000000002
test_histogram_sum{label1="str",label2="20"} 6.5499999999999998
`

	output, err := MmapFilesText(testhelpers.FixtureFilePath("mmap"), "histogram_proc_0-0.db")
	require.NoError(t, err)
	require.Equal(t, expectedOutput, output)
}
