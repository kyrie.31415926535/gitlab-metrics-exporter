package render

import (
	"bytes"

	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe/mmap"
)

func MmapFilesText(dir string, glob string) (string, error) {
	mgroups, err := probe.Await(mmap.NewProbe(dir, glob))
	if err != nil {
		return "", err
	}

	var buf bytes.Buffer
	err = NewRenderer().Text(mgroups, &buf)
	return buf.String(), err
}
