package testhelpers

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"path"
	"runtime"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

var (
	_, thisfile, _, _ = runtime.Caller(0)
	RootDir           = path.Join(path.Dir(thisfile), "../..")
	TestDir           = path.Join(RootDir, "test")
	CertsDir          = path.Join(TestDir, "certs")
	BinDir            = path.Join(RootDir, "bin")
)

type StopServer func() error
type RunningServer struct {
	Stop StopServer
}

func FixtureFilePath(dataPath string) string {
	return path.Join(TestDir, "data", dataPath)
}

func StartServer(args ...string) (*RunningServer, error) {
	serverBin := path.Join(BinDir, "gitlab-metrics-exporter")

	cmd := exec.Command(serverBin, args...)

	if err := cmd.Start(); err != nil {
		return nil, err
	}

	return &RunningServer{
		func() error {
			return cmd.Process.Kill()
		},
	}, nil
}

func Await(predicate func() bool) error {
	timeout := time.Duration(10) * time.Second
	elapsed := time.Duration(0)
	start := time.Now()
	for !predicate() && elapsed < timeout {
		time.Sleep(time.Duration(100) * time.Millisecond)
		elapsed = time.Since(start)
	}

	if elapsed >= timeout {
		return fmt.Errorf("timed out waiting for condition")
	}

	return nil
}

func HTTPClient(t *testing.T) *http.Client {
	return http.DefaultClient
}

func HTTPSClient(t *testing.T) *http.Client {
	certFile, err := os.ReadFile(path.Join(CertsDir, "ca.crt"))
	require.NoError(t, err)

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(certFile)

	return &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs:    caCertPool,
				MinVersion: tls.VersionTLS12,
			},
		},
	}
}
