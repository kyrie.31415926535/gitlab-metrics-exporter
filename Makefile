SHELL := /bin/bash

BUILD_DIR := bin
PREFIX ?= /usr/local
GOBUILD := go build
NAME := gitlab-metrics-exporter
BIN := $(BUILD_DIR)/$(NAME)
PKG := gitlab.com/gitlab-org/$(NAME)

.PHONY: all
all: clean $(BIN)

$(BIN):
	$(GOBUILD) -o $(BIN) $(PKG)/cmd/srv

.PHONY: install
install: $(BIN)
	install -d $(PREFIX)/bin
	install -m755 $(BIN) $(PREFIX)/bin

.PHONY: tidy
tidy:
	go fmt ./...
	go vet ./...
	go mod tidy

.PHONY: test
test: test-unit test-acceptance

.PHONY: test-unit
test-unit:
	go test -v ./internal/... ./cmd/...

.PHONY: test-acceptance
test-acceptance: $(BIN)
	bundle install --gemfile=ruby-client-mmap/Gemfile
	go test -v ./test/acceptance -run Test_Acceptance

.PHONY: verify
verify: tidy test

.PHONY: bench
bench:
	go test ./test/benchmark -bench=.

.PHONY: profile
profile:
	go test -cpuprofile cpu.prof -memprofile mem.prof ./test/benchmark -bench=.

.PHONY: clean
clean:
	rm -f bin/*
