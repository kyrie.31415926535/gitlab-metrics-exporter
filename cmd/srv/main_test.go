package main

import (
	"bytes"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe/mmap"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe/self"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/testhelpers"
)

func Test_MetricFamilyIntegrity(t *testing.T) {
	for _, dir := range []string{"mmap/prod-puma", "mmap/prod-sidekiq"} {
		probes := []probe.Probe{mmap.NewDefaultProbe(testhelpers.FixtureFilePath(dir))}
		var buf bytes.Buffer

		writeProbeResults(probes, &buf)

		lines := strings.Split(buf.String(), "\n")
		require.GreaterOrEqual(t, len(lines), 2)
		// Chop off the empty EOS line.
		lines = lines[0 : len(lines)-1]

		expectedMetric := ""
		for _, line := range lines {
			// Skip all HELP lines since they are not significant.
			if strings.HasPrefix(line, "# HELP") {
				continue
			}
			// All metrics within a family must start with the same prefix.
			if strings.HasPrefix(line, "# TYPE") {
				parts := strings.Split(line, " ")
				expectedMetric = parts[2]
				continue
			}

			require.True(t, strings.HasPrefix(line, expectedMetric), "%s: expected '%s' to match '%s.*'", dir, line, expectedMetric)
		}
	}
}

func Test_ServerConfigSetsDefaults(t *testing.T) {
	cfg, err := appConfig()

	require.NoError(t, err)
	require.Equal(t, "", cfg.host)
	require.Equal(t, defaultPort, cfg.port)
	require.Equal(t, []probe.Probe{self.NewProbe()}, cfg.probes)
}

func Test_ServerConfigSetsCustomHostAndPort(t *testing.T) {
	cfg, err := appConfig("--host", "0.0.0.0", "--port", "1234")

	require.NoError(t, err)
	require.Equal(t, "0.0.0.0", cfg.host)
	require.Equal(t, "1234", cfg.port)
}

func Test_ServerConfigSetsCustomProbes(t *testing.T) {
	cfg, err := appConfig("--probe", "self", "--probe", "mmap", "--mmapdir", "/path/to/metrics")

	require.NoError(t, err)
	require.ElementsMatch(t, []probe.Probe{
		self.NewProbe(),
		mmap.NewDefaultProbe("/path/to/metrics"),
	}, cfg.probes)
}

func Test_ServerConfigReturnsErrorWhenMmapDirNotSet(t *testing.T) {
	_, err := appConfig("--probe", "mmap")

	require.ErrorContains(t, err, "cannot be blank: --mmapdir")
}

func appConfig(flags ...string) (config, error) {
	var cfg config
	var err error
	app := buildCliApp()
	app.Action = func(ctx *cli.Context) error {
		cfg, err = loadConfig(ctx)
		return nil
	}

	var args []string
	args = append(args, "server")
	args = append(args, flags...)

	app.Run(args)

	return cfg, err
}
