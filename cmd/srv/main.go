package main

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io"
	"net/http"
	_ "net/http/pprof"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"

	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe/mmapstats"

	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe/mmap"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe/self"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/render"
	"golang.org/x/sync/singleflight"

	goLog "log"

	"gitlab.com/gitlab-org/labkit/correlation"
	"gitlab.com/gitlab-org/labkit/log"
	"gitlab.com/gitlab-org/labkit/metrics"

	"github.com/urfave/cli/v2"
)

const (
	metricsEndpoint = "/metrics"

	probeSelf      = "self"
	probeMmap      = "mmap"
	probeMmapStats = "mmap_stats"

	defaultPort      = "8082"
	defaultLogFile   = "stderr"
	defaultLogFormat = "json"
	defaultLogLevel  = "info"

	// Go sets these timeouts to 0, which means unlimited.
	readTimeout = 3 * time.Second
	// Choose a high enough write timeout for pprof requests to work.
	writeTimeout = 5 * time.Minute
)

var (
	defaultProbes   = []string{probeSelf}
	availableProbes = []string{probeSelf, probeMmap, probeMmapStats}
)

type config struct {
	host      string
	port      string
	certFile  string
	certKey   string
	probes    []probe.Probe
	logFile   string
	logLevel  string
	logFormat string
}

// We limit client connections to 1 so as to favor low CPU and memory use over high throughput.
// This implies that if a scraper request is in-flight, any concurrent requests will be enqueued
// and served the same result as the ongoing request.
type probeHandler struct {
	group  singleflight.Group
	probes []probe.Probe
}

func main() {
	// enable mutex profiling on 1% of mutex contention events
	// matching the settings we use in labkit and Google Cloud Profiler
	// - https://gitlab.com/gitlab-org/labkit/-/blob/master/monitoring/profiler.go#L45
	// - https://github.com/googleapis/google-cloud-go/blob/5a2ed6b2cd1c304e0f59daa29959863bff9b5c29/profiler/mutex.go#L21
	runtime.SetMutexProfileFraction(100)

	err := buildCliApp().Run(os.Args)
	if err != nil {
		goLog.Fatalf("server terminated: %v", err)
	}
}

func buildCliApp() *cli.App {
	return &cli.App{
		Usage: "A Prometheus exporter for GitLab application metrics",
		Flags: buildCliFlags(),
		Action: func(c *cli.Context) error {
			cfg, err := loadConfig(c)
			if err != nil {
				return err
			}
			return startServer(&cfg)
		},
	}
}

func buildCliFlags() []cli.Flag {
	sharedFlags := []cli.Flag{
		&cli.StringFlag{
			Name:    "host",
			Usage:   "TCP server address host",
			EnvVars: []string{"GME_SERVER_HOST"},
		},
		&cli.StringFlag{
			Name:    "port",
			Value:   defaultPort,
			Usage:   "TCP server address port",
			EnvVars: []string{"GME_SERVER_PORT"},
		},
		&cli.StringFlag{
			Name:    "cert-file",
			Usage:   "Path to TLS certificate PEM file (combined format); requires --cert-key to be set",
			EnvVars: []string{"GME_CERT_FILE"},
		},
		&cli.StringFlag{
			Name:    "cert-key",
			Usage:   "Path to TLS certificate key; requires --cert-file to be set",
			EnvVars: []string{"GME_CERT_KEY"},
		},
		&cli.StringSliceFlag{
			Name:    "probe",
			Value:   cli.NewStringSlice(defaultProbes...),
			Usage:   "probe to run when metrics are requested (available: " + strings.Join(availableProbes, ", ") + ")",
			EnvVars: []string{"GME_PROBES"},
			Aliases: []string{"p"},
		},
		&cli.StringFlag{
			Name:    "log-file",
			Value:   defaultLogFile,
			Usage:   "Path to a file receiving log output (including stdout, stderr)",
			EnvVars: []string{"GME_LOG_FILE"},
		},
		&cli.StringFlag{
			Name:    "log-format",
			Value:   defaultLogFormat,
			Usage:   "One of text, color, json",
			EnvVars: []string{"GME_LOG_FORMAT"},
		},
		&cli.StringFlag{
			Name:    "log-level",
			Value:   defaultLogLevel,
			Usage:   "One of quiet, panic, fatal, error, warn, info, debug, trace",
			EnvVars: []string{"GME_LOG_LEVEL"},
		},
	}

	return append(sharedFlags, mmapProbeFlags()...)
}

func mmapProbeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    "mmapdir",
			Usage:   "mmap probe: absolute path to prometheus-client-mmap metrics files",
			EnvVars: []string{"GME_MMAP_METRICS_DIR"},
		},
	}
}

func loadConfig(c *cli.Context) (config, error) {
	cfg := config{
		host:      c.String("host"),
		port:      c.String("port"),
		certFile:  c.String("cert-file"),
		certKey:   c.String("cert-key"),
		logFile:   c.String("log-file"),
		logFormat: c.String("log-format"),
		logLevel:  c.String("log-level"),
	}

	probes, err := loadProbes(c)
	if err != nil {
		return cfg, err
	}
	cfg.probes = probes

	return cfg, nil
}

func loadProbes(c *cli.Context) ([]probe.Probe, error) {
	probeNames := c.StringSlice("probe")
	var probes []probe.Probe
	for _, probeName := range probeNames {
		if !isSupportedProbe(probeName) {
			return nil, fmt.Errorf("unsupported probe: %s", probeName)
		}

		switch probeName {
		case probeSelf:
			probes = append(probes, self.NewProbe())
		case probeMmap:
			metricsDir := c.String("mmapdir")
			if metricsDir == "" {
				return nil, fmt.Errorf("cannot be blank: --mmapdir")
			}
			probes = append(probes, mmap.NewDefaultProbe(metricsDir))
		case probeMmapStats:
			metricsDir := c.String("mmapdir")
			if metricsDir == "" {
				return nil, fmt.Errorf("cannot be blank: --mmapdir")
			}
			probes = append(probes, mmapstats.NewProbe(metricsDir))
		}
	}

	return probes, nil
}

func isSupportedProbe(probe string) bool {
	for _, p := range availableProbes {
		if p == probe {
			return true
		}
	}
	return false
}

func startServer(cfg *config) error {
	logCloser, err := internal.SetupLog(cfg.logFile, cfg.logFormat, cfg.logLevel)
	if err != nil {
		return err
	}
	defer func() {
		_ = logCloser.Close()
	}()

	ph := &probeHandler{probes: cfg.probes}
	hf := metrics.NewHandlerFactory(metrics.WithLabels("path"), metrics.WithNamespace(probe.MetricsNamespace))
	http.Handle(metricsEndpoint,
		correlation.InjectCorrelationID(
			log.AccessLogger(hf(ph, metrics.WithLabelValues(map[string]string{"path": metricsEndpoint}))),
		))

	srvAddr := cfg.host + ":" + cfg.port
	log.WithField("address", srvAddr).WithField("pid", os.Getpid()).Info("server start")
	server := &http.Server{
		Addr:         srvAddr,
		WriteTimeout: writeTimeout,
		ReadTimeout:  readTimeout,
	}

	// If certificates are configured, serve via HTTPS.
	if cfg.certFile != "" && cfg.certKey != "" {
		return serveTLS(server, cfg)
	}

	// Otherwise, serve via HTTP.
	return server.ListenAndServe()
}

func (ph *probeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	respBody, err, _ := ph.group.Do("req_queue", func() (interface{}, error) {
		buf := &bytes.Buffer{}
		err := writeProbeResults(ph.probes, buf)
		return buf.Bytes(), err
	})

	if err != nil {
		log.WithContextFields(r.Context(), log.Fields{
			"pid": os.Getpid(),
		}).WithError(err).Error("error")
		w.WriteHeader(500)
		return
	}

	_, _ = w.Write(respBody.([]byte))
}

func serveTLS(server *http.Server, cfg *config) error {
	server.TLSConfig = &tls.Config{
		MinVersion: tls.VersionTLS12,
	}

	return server.ListenAndServeTLS(cfg.certFile, cfg.certKey)
}

func writeProbeResults(probes []probe.Probe, w io.Writer) error {
	ch := make(chan probe.ProbeResult)

	wg := sync.WaitGroup{}

	for _, p := range probes {
		wg.Add(1)

		p_ := p
		go func() {
			defer wg.Done()
			if err := p_.Probe(ch); err != nil {
				log.WithField("probe", p_.String()).Errorf("Probe: %v", err)
			}
		}()
	}

	go func() {
		defer close(ch)
		wg.Wait()
	}()

	return render.NewRenderer().TextAsync(ch, w)
}
