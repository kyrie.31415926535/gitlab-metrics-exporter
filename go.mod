module gitlab.com/gitlab-org/gitlab-metrics-exporter

go 1.17

require (
	github.com/prometheus/client_model v0.2.0
	github.com/prometheus/common v0.33.0
	github.com/stretchr/testify v1.7.1
)

require (
	github.com/prometheus/client_golang v1.12.1
	github.com/urfave/cli/v2 v2.4.0
	gitlab.com/gitlab-org/labkit v1.12.0
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/client9/reopen v1.0.0 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/oklog/ulid/v2 v2.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/prometheus/procfs v0.7.3 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/sebest/xff v0.0.0-20210106013422-671bd2870b3a // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	golang.org/x/sys v0.0.0-20220405052023-b1e9470b6e64 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
