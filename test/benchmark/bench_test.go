package benchmark

import (
	"io"
	"io/fs"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe/mmap"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/render"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/testhelpers"
)

func Benchmark_Stage1_ParseAllFiles(b *testing.B) {
	b.ReportAllocs()

	dirPath := testhelpers.FixtureFilePath("mmap/prod-puma")
	fileSystem := os.DirFS(dirPath)
	files, err := fs.Glob(fileSystem, "*.db")
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		for _, file := range files {
			filePath := path.Join(dirPath, file)
			metricFile, err := mmap.ParseFile(filePath)
			require.NoError(b, err)
			require.NotEmpty(b, metricFile.Samples)
		}
	}
}

func Benchmark_Stage2_MmapProbeAll(b *testing.B) {
	b.ReportAllocs()

	p := mmap.NewDefaultProbe(testhelpers.FixtureFilePath("mmap/prod-puma"))
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		mgroups, err := probe.Await(p)
		require.NoError(b, err)
		require.NotEmpty(b, mgroups)
	}
}

func Benchmark_Stage3_MmapRenderAll(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		output, err := render.MmapFilesText(testhelpers.FixtureFilePath("mmap/prod-puma"), "*.db")
		require.NoError(b, err)
		require.NotEmpty(b, output)
	}
}

func Benchmark_ParseCounterFile(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		f, err := mmap.ParseFile(testhelpers.FixtureFilePath("mmap/prod-puma/counter_puma_0-0.db"))
		require.NoError(b, err)
		require.NotEmpty(b, f.Samples)
	}
}

func Benchmark_ParseHistogramFile(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		f, err := mmap.ParseFile(testhelpers.FixtureFilePath("mmap/prod-puma/histogram_puma_0-0.db"))
		require.NoError(b, err)
		require.NotEmpty(b, f.Samples)
	}
}

func Benchmark_RenderMmapText(b *testing.B) {
	r := render.NewRenderer()
	b.ReportAllocs()

	p := mmap.NewDefaultProbe(testhelpers.FixtureFilePath("mmap/prod-puma"))
	mgroups, err := probe.Await(p)
	require.NoError(b, err)
	require.NotEmpty(b, mgroups)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err = r.Text(mgroups, io.Discard)
		require.NoError(b, err)
	}
}
