package acceptance

import (
	"bytes"
	"os/exec"
	"sort"
	"strings"
	"testing"

	dto "github.com/prometheus/client_model/go"

	"github.com/prometheus/common/expfmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe/mmap"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/render"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/testhelpers"
)

var (
	scriptPath = testhelpers.RootDir + "/ruby-client-mmap/"
)

func Test_Acceptance_SameCounterOutput(t *testing.T) {
	filename := "counter_proc_0-0.db"
	expected := mmapClientSerializeFile(t, filename)
	actual := exporterSerializeFile(t, filename)

	require.Equal(t, expected, actual)
}

func Test_Acceptance_SameGaugeAggregateAllOutput(t *testing.T) {
	filename := "gauge_all_proc_0-0.db"
	expected := mmapClientSerializeFile(t, filename)
	actual := exporterSerializeFile(t, filename)

	require.Equal(t, expected, actual)
}

func Test_Acceptance_SameHistogramOutput(t *testing.T) {
	filename := "histogram_proc_0-0.db"
	expected := mmapClientSerializeFile(t, filename)
	actual := exporterSerializeFile(t, filename)

	require.Equal(t, expected, actual)
}

func Test_Acceptance_DirectoryDataMatches(t *testing.T) {
	expected := mmapClientDecodeAll(t, "mmap")
	actual := exporterDecodeAll(t, "mmap")

	requireMetricFamiliesMatch(t, expected, actual)
}

func Test_Acceptance_DirectoryDataMatchesForProductionPumaData(t *testing.T) {
	expected := mmapClientDecodeAll(t, "mmap/prod-puma")
	actual := exporterDecodeAll(t, "mmap/prod-puma")

	requireMetricFamiliesMatch(t, expected, actual)
}

func Test_Acceptance_DirectoryDataMatchesForProductionSidekiqData(t *testing.T) {
	expected := mmapClientDecodeAll(t, "mmap/prod-sidekiq")
	actual := exporterDecodeAll(t, "mmap/prod-sidekiq")

	requireMetricFamiliesMatch(t, expected, actual)
}

func decodeOutput(t *testing.T, output string) map[string]*dto.MetricFamily {
	// sort metrics to ensure the same order of all with labels
	lines := strings.Split(output, "\n")
	sort.Strings(lines)
	output = strings.Join(lines, "\n") + "\n"

	parser := expfmt.TextParser{}
	res, err := parser.TextToMetricFamilies(strings.NewReader(output))
	require.NoError(t, err)
	return res
}

func mmapClientSerializeFile(t *testing.T, filename string) string {
	return runMmapClient(t, "serialize_file.rb", testhelpers.TestDir+"/data/mmap/"+filename)
}

func mmapClientDecodeAll(t *testing.T, dir string) map[string]*dto.MetricFamily {
	return decodeOutput(t, runMmapClient(t, "serialize_dir.rb", testhelpers.TestDir+"/data/"+dir))
}

func runMmapClient(t *testing.T, script string, args ...string) string {
	cmd := exec.Command("bundle", append([]string{"exec", scriptPath + script}, args...)...)

	var out bytes.Buffer
	cmd.Dir = scriptPath
	cmd.Stdout = &out
	cmd.Stderr = &out
	err := cmd.Run()
	require.NoError(t, err, out.String())

	return out.String()
}

func exporterSerializeFile(t *testing.T, filename string) string {
	output, err := render.MmapFilesText(testhelpers.FixtureFilePath("mmap"), filename)
	require.NoError(t, err)

	return output
}

func exporterDecodeAll(t *testing.T, dir string) map[string]*dto.MetricFamily {
	p := mmap.NewDefaultProbe(testhelpers.FixtureFilePath(dir))
	metrics, err := probe.Await(p)
	require.NoError(t, err)

	var buf bytes.Buffer
	err = render.NewRenderer().Text(metrics, &buf)
	require.NoError(t, err)

	return decodeOutput(t, buf.String())
}

func findMatchingMetric(metrics []*dto.Metric, labels []*dto.LabelPair) *dto.Metric {
	for _, metric := range metrics {
		if len(metric.Label) != len(labels) {
			continue
		}

		for _, label := range labels {
			for _, metricLabel := range metric.Label {
				if assert.ObjectsAreEqual(label, metricLabel) {
					goto foundLabel
				}
			}

			goto missingLabel

		foundLabel:
		}

		return metric

	missingLabel:
	}

	return nil
}

func requireMetricFamiliesMatch(t *testing.T, expected, actual map[string]*dto.MetricFamily) {
	requireSameKeys(t, expected, actual)

	for k := range expected {
		expectedMF := expected[k]
		actualMF := actual[k]
		require.Equal(t, expectedMF.Name, actualMF.Name)
		require.Equal(t, expectedMF.Help, actualMF.Help)
		require.Equal(t, expectedMF.Type, actualMF.Type)
		require.Equal(t, len(expectedMF.Metric), len(actualMF.Metric))

		for _, m1 := range expectedMF.Metric {
			m2 := findMatchingMetric(actualMF.Metric, m1.Label)

			require.NotNil(t, m2, "Labels not found for", *expectedMF.Name, m1.Label)
			require.Equal(t, m1.Counter, m2.Counter)
			require.Equal(t, m1.Gauge, m2.Gauge)
			require.Equal(t, m1.Histogram, m2.Histogram)
			require.Equal(t, m1.Summary, m2.Summary)
			require.Equal(t, m1.Untyped, m2.Untyped)
			require.ElementsMatch(t, m1.Label, m2.Label)
		}
	}
}

func requireSameKeys(t *testing.T, m1, m2 map[string]*dto.MetricFamily) {
	keys1 := make([]string, len(m1))
	for k := range m1 {
		keys1 = append(keys1, k)
	}
	keys2 := make([]string, len(m2))
	for k := range m2 {
		keys2 = append(keys2, k)
	}
	require.ElementsMatch(t, keys1, keys2)
}
